This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Vending machine project
=======================

Notes:

1. This was a fun project!
2. Shouldn't have picked learning CSS Grid while doing this.
3. I _really_ wanted to have the LCD display, but since I couldn't find a proper NPM module, `index.html` looks kinda crap
4. I usually do test-after (empirically it has been shown that it's the same effectiveness as TDD) since I don't usually let tests drive my design except for technologies I master and have no surprises in terms of development. But for this project, I've now run out of time. However, I do believe test coverage would not dramatically improve my confidence in the code given how simple it is and that for me is the barometer for adding tests. If I had a bit more time, I'd add tests for the coin logic, reducers and actions thunks (in that order).
5. I "cheated" in the coin change logic - I know it's a standard problem in CS, but I took shortcuts since we have a finite set of coins, which is usually not how the problem is phrased. I still think my solution is adequate, of course.
6. I'm learning React and Redux, so any feedback you may have is greatly appreciated!
7. App is live at http://vending-machine.dokku.nootch.net
