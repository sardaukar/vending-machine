export const DEPOSIT_COIN = 'DEPOSIT_COIN'
export const RETURN_ALL_COINS = 'RETURN_ALL_COINS'
export const UPDATE_LCD = 'UPDATE_LCD'

export const ITEM_SELECTED = 'ITEM_SELECTED'
export const ITEM_DESELECTED = 'ITEM_DESELECTED'
export const BUY_ITEM = 'BUY_ITEM'
export const RESTOCK_ITEMS = 'RESTOCK_ITEMS'
export const RESTOCK_CHANGE = 'RESTOCK_CHANGE'
