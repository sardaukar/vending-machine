import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'

import './App.css'
import * as fundsActions from '../actions/funds'
import * as machineActions from '../actions/machine'
import { coinName, formatCurrency, sumCoinCollection } from '../utils'

import Item from './App/Item'

class App extends Component {
  itemsRow(rowNr) {
    const { items } = this.props.machine
    const marker = 3 * rowNr

    return _.slice(items, marker, marker + 3)
  }

  updateLCD() {
    const { lcdValue } = this.props.machine

    if (lcdValue != null) {
      window.jQuery('#lcd').sevenSeg({digits: 4, value: formatCurrency(lcdValue)})
    } else {
      window.jQuery('#lcd').sevenSeg({digits: 4, value: 0})
    }
  }

  render() {
    const { depositCoin, returnAllCoins } = this.props.actions.funds
    const { restockItems, restockChange } = this.props.actions.machine

    const { inCoinReceptacle, insideMachine } = this.props.funds
    const { itemCurrentlySelected } = this.props.machine

    this.updateLCD()

    return (
      <div className="App">

        <header className="App-header">
          <h1 className="App-title">Get your late night snacks here!</h1>
        </header>

        <div className="vending-machine">
          <div className="grid-container">
            <div className="frame" />
            <div className="frame" />
            <div className="frame" />

            <div className="frame" />
            <div className="item-holder">
              {
                _.map(this.itemsRow(0), (item, idx) => {
                  return <Item
                    item={item}
                    key={idx}
                    isSelected={item === itemCurrentlySelected}
                  />
                })
              }
            </div>
            <div className="frame" />

            <div className="frame" />
            <div className="item-holder">
            {
              _.map(this.itemsRow(1), (item, idx) => {
                  return <Item
                    item={item}
                    key={idx}
                    isSelected={item === itemCurrentlySelected}
                  />
                })
            }
            </div>
            <div className="frame">
              <p>
                {
                  itemCurrentlySelected ? 'PLEASE INSERT' : 'PLEASE SELECT AN ITEM'
                }
              </p>
              <div id="lcd" />
            </div>

            <div className="frame" />
            <div className="item-holder">
            {
              _.map(this.itemsRow(2), (item, idx) => {
                  return <Item
                    item={item}
                    key={idx}
                    isSelected={item === itemCurrentlySelected}
                  />
                })
            }
            </div>
            <div className="frame" />

            <div className="frame" />
            <div className="frame" />
            <div className="frame" />
          </div>
        </div>

        <div className="actions">
          <button onClick={restockItems}>RESTOCK ITEMS</button>
          <button onClick={restockChange}>RESTOCK CHANGE</button>
        </div>

        <div className="change-inspector">
          <p>
            Current change:
          </p>
          <ul className="change-list">
            {
              _.map(insideMachine, kind => {
                return(
                  <li key={kind.denomination}>
                    {coinName(kind.denomination)}({kind.amount})
                  </li>
                )
              })
            }
          </ul>
        </div>

        {
          itemCurrentlySelected &&

            [
              <div className="hand" key="hand">
                <p>Money in coin slot</p>
                <p className="big-text">
                  &pound;{formatCurrency(sumCoinCollection(inCoinReceptacle))}
                  </p>
                <button onClick={() => returnAllCoins()}>RETURN ALL COINS</button>
              </div>,

              <div className="wallet" key="wallet">
                <p className="wallet-header">Money in wallet</p>
                <ul className="wallet-picker">
                  <li onClick={() => depositCoin(1)} className="copper">1p</li>
                  <li onClick={() => depositCoin(2)} className="copper">2p</li>
                  <li onClick={() => depositCoin(5)} className="metal">5p</li>
                  <li onClick={() => depositCoin(10)} className="metal">10p</li>
                  <li onClick={() => depositCoin(20)} className="metal">20p</li>
                  <li onClick={() => depositCoin(50)} className="metal">50p</li>
                  <li onClick={() => depositCoin(100)} className="pound-coin">&pound;1</li>
                  <li onClick={() => depositCoin(200)} className="pound-coin">&pound;2</li>
                </ul>
              </div>
            ]
        }

      </div>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    machine: state.machine,
    funds: state.funds,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      funds: bindActionCreators(fundsActions, dispatch),
      machine: bindActionCreators(machineActions, dispatch),
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
