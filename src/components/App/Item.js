import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import cw from 'classwrap'

import * as machineActions from '../../actions/machine'
import { formatCurrency } from '../../utils'

class Item extends Component {
  render() {
    const { item, isSelected } = this.props
    const { pickItem } = this.props.actions.machine

    if (item.amount > 0) {
      return (
        <div
          onClick={() => pickItem(item)}
          className={
            cw({
              selected: isSelected,
              item: true
            })
          }
          key={item.id}
        >
          <p>{item.name}</p>
          <p>({item.amount})</p>
          <p>&pound;{formatCurrency(item.price)}</p>
        </div>
      )
    } else {
      return (
        <div
          className={
            cw({
              item: true,
              soldOut: true
            })
          }
          key={item.id}
        >
          <p>{item.name}</p>
          <p>Out of stock!</p>
        </div>
      )
    }
  }
}

function mapStateToProps(state, props) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      machine: bindActionCreators(machineActions, dispatch),
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Item)
