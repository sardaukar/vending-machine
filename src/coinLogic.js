import _ from 'lodash'

function getCoinRowAndIndex(coinsAvailable, coin) {
  const row = _.find(coinsAvailable, {denomination: coin.denomination})
  const rowIndex = _.findIndex(coinsAvailable, {denomination: coin.denomination})

  return { row, rowIndex }
}

function consumeCoin(coinsAvailable, coin) {
  const { row, rowIndex } = getCoinRowAndIndex(coinsAvailable, coin)

  coinsAvailable.splice(rowIndex, 1, {...row, amount: row.amount - 1} )

  return coinsAvailable
}

function addCoin(coin, coinsAvailable) {
  const { row, rowIndex } = getCoinRowAndIndex(coinsAvailable, coin)

  coinsAvailable.splice(rowIndex, 1, {...row, amount: row.amount + 1} )

  return coinsAvailable
}

function matchCoin(amount, coinsAvailable) {
  const foundCoin = _.findLast(coinsAvailable, (coin) => {
    return coin.amount > 0 && coin.denomination <= amount
  })

  return foundCoin
}

function makeChange(amount, coinsAvailable) {
  let changeCoins = []
  let isShort = false

  while (amount > 0) {
    const nextCoin = matchCoin(amount, coinsAvailable)

    if (nextCoin) {
      changeCoins = changeCoins.concat(nextCoin)
      coinsAvailable = consumeCoin(coinsAvailable, nextCoin)

      amount -= nextCoin.denomination

    } else {
      isShort = true
      break
    }
  }

  return {
    changeCoins,
    newCoinsInsideMachine: coinsAvailable,
    isShort
  }
}


export function acceptCoinsForItem(coinsInsideMachine, coinsForItem, item) {
  const amountPaid = _.reduce(coinsForItem, (sum, elem) => {
    return sum + elem.denomination
  }, 0)

  const changeAmount = amountPaid - item.price

  if (changeAmount > 0 ) {
    let {
      changeCoins,
      newCoinsInsideMachine,
      isShort,
    } = makeChange(changeAmount, coinsInsideMachine)

    _.map(coinsForItem, coin => {
      newCoinsInsideMachine = addCoin(coin, newCoinsInsideMachine)
    })

    return {
      newCoinsInsideMachine: newCoinsInsideMachine,
      change: {
        amount: changeAmount,
        coins: changeCoins,
        isShort: isShort,
      }
    }
  } else {

    _.map(coinsForItem, coin => {
      coinsInsideMachine = addCoin(coin, coinsInsideMachine)
    })

    return {
      newCoinsInsideMachine: coinsInsideMachine,
      change: {
        amount: 0,
        coins: [],
        isShort: false,
      }
    }
  }
}
