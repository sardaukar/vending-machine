import { combineReducers } from 'redux'

import funds from './funds'
import machine from './machine'

const rootReducer = combineReducers({
  funds,
  machine
})

export default rootReducer
