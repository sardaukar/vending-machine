import _ from 'lodash'

import {
  ITEM_SELECTED,
  ITEM_DESELECTED,
  BUY_ITEM,
  UPDATE_LCD,
  RESTOCK_ITEMS,
} from '../constants'

function decreaseStockForItem(items, boughtItem) {
  const newItems = _.map(items, item => {
    if (item === boughtItem) {
      return {...item, amount: item.amount - 1}
    } else {
      return item
    }
  })

  return newItems
}

export default(state = {}, action) => {
  switch(action.type) {
    case RESTOCK_ITEMS:
      return {
        ...state,
        items: [
          {name: 'Crisps', price: 160, amount: 10, id: 1},
          {name: 'Crisps (vinegar)', price: 180, amount: 10, id: 2},
          {name: 'Crisps (cheddar)', price: 190, amount: 10, id: 3},
          {name: 'Haribo', price: 90, amount: 10, id: 4},
          {name: 'Reese\'s PB cups', price: 340, amount: 10, id: 5},
          {name: 'Galak Milk Chocolate', price: 110, amount: 10, id: 6},
          {name: 'Evian (33cl)', price: 260, amount: 10, id: 7},
          {name: 'Evian (33cl)', price: 260, amount: 10, id: 8},
          {name: 'Beluga', price: 1490, amount: 2, id: 9},
        ],
      }

    case UPDATE_LCD:
      return {
        ...state,
        lcdValue: action.payload.value,
      }

    case BUY_ITEM:
      const newItems = decreaseStockForItem(state.items, action.payload.item)

      return {
        ...state,
        items: newItems,
        itemCurrentlySelected: null,
      }

    case ITEM_SELECTED:
      return {...state, itemCurrentlySelected: action.payload.item}

    case ITEM_DESELECTED:
      return {...state, itemCurrentlySelected: null}

    default:
      return state
  }
}
