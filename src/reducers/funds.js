import _ from 'lodash'

import {
  DEPOSIT_COIN,
  BUY_ITEM,
  RETURN_ALL_COINS,
  RESTOCK_CHANGE,
} from '../constants'

import { coinName } from '../utils'
import { acceptCoinsForItem } from '../coinLogic'

function changeMessage(change) {
  alert(`Thank you for your business! Enjoy!`)

  if (change.coins.length > 0) {
    const changeCoinsString = _.map(change.coins, coin => {
      return `[${coinName(coin.denomination)}]`
    }).join()

    alert(`Your change is: ${changeCoinsString}`)
  }

  if (change.isShort) {
    alert(`We apologize, but we couldn't return all of your due change`)
  }
}

export default(state = {}, action) => {
  switch(action.type) {
    case RESTOCK_CHANGE:
      return {
        ...state,
        insideMachine: [
          {denomination: 1, amount: 10},
          {denomination: 2, amount: 10},
          {denomination: 5, amount: 10},
          {denomination: 10, amount: 10},
          {denomination: 20, amount: 10},
          {denomination: 50, amount: 10},
          {denomination: 100, amount: 10},
          {denomination: 200, amount: 10},
        ],
      }

    case RETURN_ALL_COINS:
      return {
        ...state,
        inCoinReceptacle: [],
      }

    case BUY_ITEM:
      const coinsInsideMachine = state.insideMachine
      const coinsInCoinReceptacle = state.inCoinReceptacle

      const { newCoinsInsideMachine, change } = acceptCoinsForItem(
        coinsInsideMachine,
        coinsInCoinReceptacle,
        action.payload.item,
      )

      changeMessage(change)

      return {
        ...state,
        inCoinReceptacle: [],
        insideMachine: newCoinsInsideMachine
      }

    case DEPOSIT_COIN:
      const newCoins = state.inCoinReceptacle.concat({denomination: action.payload.denomination})

      return {...state, inCoinReceptacle: newCoins}

    default:
      return state
  }
}
