import {
  DEPOSIT_COIN,
  RETURN_ALL_COINS,
} from '../constants'

import * as machineActions from './machine'
import { sumCoinCollection } from '../utils'

function doDepositCoin(denomination) {
  return {
    type: DEPOSIT_COIN,
    payload: {
      denomination
    },
  }
}

function doReturnAllCoins() {
  return {
    type: RETURN_ALL_COINS,
  }
}

export function returnAllCoins() {
  return (dispatch, getState) => {
    dispatch(doReturnAllCoins())

    const { itemCurrentlySelected } = getState().machine

    if ( itemCurrentlySelected != null ) {
      dispatch(machineActions.updateLCD(itemCurrentlySelected.price))
    } else {
      dispatch(machineActions.updateLCD())
    }
  }
}

export const depositCoin = (denomination) => {
  return (dispatch, getState) => {
    dispatch(doDepositCoin(denomination))

    const { itemCurrentlySelected } = getState().machine

    if ( itemCurrentlySelected != null ) {
      const { inCoinReceptacle } = getState().funds
      const total = sumCoinCollection(inCoinReceptacle)

      const moneyToGo = itemCurrentlySelected.price - total

      if (moneyToGo <= 0) {
        dispatch(machineActions.buyItem(itemCurrentlySelected))
        dispatch(machineActions.updateLCD())
      } else {
        dispatch(machineActions.updateLCD(moneyToGo))
      }
    }
  }
}
