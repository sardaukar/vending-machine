import {
  ITEM_SELECTED,
  ITEM_DESELECTED,
  BUY_ITEM,
  UPDATE_LCD,
  RESTOCK_ITEMS,
  RESTOCK_CHANGE,
} from '../constants'

export const itemSelected = (item) => {
  return {
    type: ITEM_SELECTED,
    payload: {
      item
    },
  }
}

export const itemDeselected = (item) => {
  return {
    type: ITEM_DESELECTED,
    payload: {
      item
    },
  }
}

export function buyItem(item) {
  return {
    type: BUY_ITEM,
    payload: {
      item
    },
  }
}

export function updateLCD(value = null) {
  return {
    type: UPDATE_LCD,
    payload: {
      value
    },
  }
}

export function restockItems() {
  return {
    type: RESTOCK_ITEMS,
  }
}

export function restockChange() {
  return {
    type: RESTOCK_CHANGE,
  }
}

export function pickItem(item) {
  return (dispatch, getState) => {
    const { itemCurrentlySelected } = getState().machine

    if ( itemCurrentlySelected == null ) {
      dispatch(itemSelected(item))
      dispatch(updateLCD(item.price))
    } else {
      if ( itemCurrentlySelected === item ) {
        dispatch(itemDeselected(item))
        dispatch(updateLCD())
      } else {
        dispatch(itemSelected(item))
        dispatch(updateLCD(item.price))
      }
    }
  }
}

