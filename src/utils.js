export function formatCurrency(number) {
  return Number(number / 100).toFixed(2)
}

export function sumCoinCollection(coinCollection) {
  const total = coinCollection.reduce( (sum, coin) => {
    return sum + coin.denomination;
  }, 0)

  return total
}

export function coinName(denomination) {
  switch(denomination) {
    case 1: return '1p'
    case 2: return '2p'
    case 5: return '5p'
    case 10: return '10p'
    case 20: return '20p'
    case 50: return '50p'
    case 100: return '£1'
    case 200: return '£2'
    default: throw new Error('this never happens')
  }
}
