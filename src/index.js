import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import './index.css'
import Store from './store'
import App from './components/App'

const initialState = {
  funds: {
    inCoinReceptacle: [],
    insideMachine: [
      {denomination: 1, amount: 10},
      {denomination: 2, amount: 10},
      {denomination: 5, amount: 10},
      {denomination: 10, amount: 10},
      {denomination: 20, amount: 10},
      {denomination: 50, amount: 10},
      {denomination: 100, amount: 10},
      {denomination: 200, amount: 10},
    ],
  },
  machine: {
    lcdValue: null,
    items: [
      {name: 'Crisps', price: 160, amount: 10, id: 1},
      {name: 'Crisps (vinegar)', price: 180, amount: 10, id: 2},
      {name: 'Crisps (cheddar)', price: 190, amount: 10, id: 3},
      {name: 'Haribo', price: 90, amount: 10, id: 4},
      {name: 'Reese\'s PB cups', price: 340, amount: 10, id: 5},
      {name: 'Galak Milk Chocolate', price: 110, amount: 10, id: 6},
      {name: 'Evian (33cl)', price: 260, amount: 10, id: 7},
      {name: 'Evian (33cl)', price: 260, amount: 10, id: 8},
      {name: 'Beluga', price: 1490, amount: 2, id: 9},
    ],
    itemCurrentlySelected: null
  },
}

const StoreInstance = Store(initialState)

ReactDOM.render(
  <Provider store={StoreInstance}>
    <App />
  </Provider>,
  document.getElementById('root')
)
